/********
* ec2c version 0.68
* c main file generated for node : PORTE 
* to be used with : PORTE.c 
* and             : PORTE.h 
* context   method = HEAP
* ext call  method = PROCEDURES
********/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "PORTE.h"

/* Print a promt ? ************************/
static int ISATTY;
/* MACROS DEFINITIONS ****************/
#ifndef TT
#define TT "true"
#endif
#ifndef FF
#define FF "false"
#endif
#ifndef BB
#define BB "bottom"
#endif
#ifdef CKCHECK
/* set this macro for testing output clocks */
#endif

/* Standard Input procedures **************/
_boolean _get_bool(char* n){
   char b[512];
   _boolean r = 0;
   int s = 1;
   char c;
   do {
      if(ISATTY) {
         if((s != 1)||(r == -1)) printf("\a");
         printf("%s (1,t,T/0,f,F) ? ", n);
      }
      if(scanf("%s", b)==EOF) exit(0);
      s = sscanf(b, "%c", &c);
      r = -1;
      if((c == '0') || (c == 'f') || (c == 'F')) r = 0;
      if((c == '1') || (c == 't') || (c == 'T')) r = 1;
   } while((s != 1) || (r == -1));
   return r;
}
_integer _get_int(char* n){
   char b[512];
   _integer r;
   int s = 1;
   do {
      if(ISATTY) {
         if(s != 1) printf("\a");
         printf("%s (integer) ? ", n);
      }
      if(scanf("%s", b)==EOF) exit(0);
      s = sscanf(b, "%d", &r);
   } while(s != 1);
   return r;
}
#define REALFORMAT ((sizeof(_real)==8)?"%lf":"%f")
_real _get_real(char* n){
   char b[512];
   _real r;
   int s = 1;
   do {
      if(ISATTY) {
         if(s != 1) printf("\a");
         printf("%s (real) ? ", n);
      }
      if(scanf("%s", b)==EOF) exit(0);
      s = sscanf(b, REALFORMAT, &r);
   } while(s != 1);
   return r;
}
/* Standard Output procedures **************/
void _put_bottom(char* n){
   if(ISATTY) printf("%s = ", n);
   printf("%s ", BB);
   if(ISATTY) printf("\n");
}
void _put_bool(char* n, _boolean _V){
   if(ISATTY) printf("%s = ", n);
   printf("%s ", (_V)? TT : FF);
   if(ISATTY) printf("\n");
}
void _put_int(char* n, _integer _V){
   if(ISATTY) printf("%s = ", n);
   printf("%d ", _V);
   if(ISATTY) printf("\n");
}
void _put_real(char* n, _real _V){
   if(ISATTY) printf("%s = ", n);
   printf("%f ", _V);
   if(ISATTY) printf("\n");
}
/* Output procedures **********************/
#ifdef CKCHECK
void PORTE_BOT_ouvrir_porte(void* cdata){
   _put_bottom("ouvrir_porte");
}
void PORTE_BOT_fermer_porte(void* cdata){
   _put_bottom("fermer_porte");
}
void PORTE_BOT_porte_ok(void* cdata){
   _put_bottom("porte_ok");
}
#endif
/* Output procedures **********************/
void PORTE_O_ouvrir_porte(void* cdata, _boolean _V) {
   _put_bool("ouvrir_porte", _V);
}
void PORTE_O_fermer_porte(void* cdata, _boolean _V) {
   _put_bool("fermer_porte", _V);
}
void PORTE_O_porte_ok(void* cdata, _boolean _V) {
   _put_bool("porte_ok", _V);
}/* Main procedure *************************/
int main(){
   
   int s = 0;
   /* Context allocation */
   struct PORTE_ctx* ctx = PORTE_new_ctx(NULL);
   PORTE_reset(ctx);
   /* Main loop */
   ISATTY = isatty(0);
   while(1){
      if (ISATTY) printf("## STEP %d ##########\n", s+1);
      else if(s) printf("\n");
      fflush(stdout);
      ++s;
      PORTE_I_en_station(ctx, _get_bool("en_station"));
      PORTE_I_attention_depart(ctx, _get_bool("attention_depart"));
      PORTE_I_demande_porte(ctx, _get_bool("demande_porte"));
      PORTE_I_porte_ouverte(ctx, _get_bool("porte_ouverte"));
      PORTE_step(ctx);
      
   }
   return 1;
   
}
