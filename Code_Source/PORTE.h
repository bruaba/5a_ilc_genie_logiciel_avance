/********
* ec2c version 0.68
* context   method = HEAP
* ext call  method = PROCEDURES
* c header file generated for node : PORTE 
* to be used with : PORTE.c 
********/
#ifndef _PORTE_EC2C_H_FILE
#define _PORTE_EC2C_H_FILE
/*-------- Predefined types ---------*/
#ifndef _EC2C_PREDEF_TYPES
#define _EC2C_PREDEF_TYPES
typedef int _boolean;
typedef int _integer;
typedef char* _string;
typedef double _real;
typedef double _double;
typedef float _float;
#define _false 0
#define _true 1
#endif
/*--------- Pragmas ----------------*/
//MODULE: PORTE 4 3
//IN: _boolean en_station
//IN: _boolean attention_depart
//IN: _boolean demande_porte
//IN: _boolean porte_ouverte
//OUT: _boolean ouvrir_porte
//OUT: _boolean fermer_porte
//OUT: _boolean porte_ok
#ifndef _PORTE_EC2C_SRC_FILE
/*--------Context type -------------*/
struct PORTE_ctx;
/*-------- Input procedures -------------*/
extern void PORTE_I_en_station(struct PORTE_ctx* ctx, _boolean);
extern void PORTE_I_attention_depart(struct PORTE_ctx* ctx, _boolean);
extern void PORTE_I_demande_porte(struct PORTE_ctx* ctx, _boolean);
extern void PORTE_I_porte_ouverte(struct PORTE_ctx* ctx, _boolean);
/*-------- Reset procedure -----------*/
extern void PORTE_reset(struct PORTE_ctx* ctx);
/*--------Context copy -------------*/
extern void PORTE_copy_ctx(struct PORTE_ctx* dest, struct PORTE_ctx* src);
/*--------Context allocation --------*/
extern struct PORTE_ctx* PORTE_new_ctx(void* client_data);
/*-------- Step procedure -----------*/
extern void PORTE_step(struct PORTE_ctx* ctx);
#endif
#endif
