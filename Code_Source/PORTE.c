/********
* ec2c version 0.68
* c file generated for node : PORTE 
* context   method = HEAP
* ext call  method = PROCEDURES
********/
#include <stdlib.h>
#include <string.h>
#define _PORTE_EC2C_SRC_FILE
#include "PORTE.h"
/*--------
Internal structure for the call
--------*/
typedef struct  {
   void* client_data;
   //INPUTS
   _boolean _en_station;
   _boolean _attention_depart;
   _boolean _demande_porte;
   _boolean _porte_ouverte;
   //OUTPUTS
   _boolean _ouvrir_porte;
   _boolean _fermer_porte;
   _boolean _porte_ok;
   //REGISTERS
} PORTE_ctx;
/*--------
Output procedures must be defined,
Input procedures must be used:
--------*/
void PORTE_I_en_station(PORTE_ctx* ctx, _boolean V){
   ctx->_en_station = V;
}
void PORTE_I_attention_depart(PORTE_ctx* ctx, _boolean V){
   ctx->_attention_depart = V;
}
void PORTE_I_demande_porte(PORTE_ctx* ctx, _boolean V){
   ctx->_demande_porte = V;
}
void PORTE_I_porte_ouverte(PORTE_ctx* ctx, _boolean V){
   ctx->_porte_ouverte = V;
}
extern void PORTE_O_ouvrir_porte(void* cdata, _boolean);
extern void PORTE_O_fermer_porte(void* cdata, _boolean);
extern void PORTE_O_porte_ok(void* cdata, _boolean);
#ifdef CKCHECK
extern void PORTE_BOT_ouvrir_porte(void* cdata);
extern void PORTE_BOT_fermer_porte(void* cdata);
extern void PORTE_BOT_porte_ok(void* cdata);
#endif
/*--------
Internal reset input procedure
--------*/
static void PORTE_reset_input(PORTE_ctx* ctx){
   //NOTHING FOR THIS VERSION...
}
/*--------
Reset procedure
--------*/
void PORTE_reset(PORTE_ctx* ctx){
   PORTE_reset_input(ctx);
}
/*--------
Copy the value of an internal structure
--------*/
void PORTE_copy_ctx(PORTE_ctx* dest, PORTE_ctx* src){
   memcpy((void*)dest, (void*)src, sizeof(PORTE_ctx));
}
/*--------
Dynamic allocation of an internal structure
--------*/
PORTE_ctx* PORTE_new_ctx(void* cdata){
   PORTE_ctx* ctx = (PORTE_ctx*)calloc(1, sizeof(PORTE_ctx));
   ctx->client_data = cdata;
   PORTE_reset(ctx);
   return ctx;
}
/*--------
Step procedure
--------*/
void PORTE_step(PORTE_ctx* ctx){
//LOCAL VARIABLES
   _boolean L9;
   _boolean L8;
   _boolean L7;
   _boolean L6;
   _boolean L5;
   _boolean L14;
   _boolean L15;
   _boolean L13;
   _boolean L12;
   _boolean L4;
   _boolean L20;
   _boolean L19;
   _boolean L21;
   _boolean L18;
   _boolean L23;
   _boolean L25;
   _boolean L24;
   _boolean L22;
   _boolean L17;
   _boolean L29;
   _boolean L28;
   _boolean L31;
   _boolean L30;
   _boolean L27;
//CODE
   if (ctx->_attention_depart) {
      L9 = _true;
   } else {
      L9 = _false;
   }
   L8 = (! L9);
   if (L8) {
      L7 = _true;
   } else {
      L7 = _false;
   }
   L6 = (ctx->_demande_porte && L7);
   if (L6) {
      L5 = _true;
   } else {
      L5 = _false;
   }
   L14 = (ctx->_en_station && ctx->_demande_porte);
   L15 = (! ctx->_porte_ouverte);
   L13 = (L14 && L15);
   if (L13) {
      L12 = _true;
   } else {
      L12 = _false;
   }
   if (L5) {
      L4 = L12;
   } else {
      L4 = _false;
   }
   PORTE_O_ouvrir_porte(ctx->client_data, L4);
   L20 = (L9 && ctx->_en_station);
   if (L20) {
      L19 = _true;
   } else {
      L19 = _false;
   }
   L21 = (ctx->_porte_ouverte || ctx->_demande_porte);
   L18 = (L19 && L21);
   L23 = (ctx->_en_station && ctx->_porte_ouverte);
   L25 = (! ctx->_en_station);
   if (L25) {
      L24 = _true;
   } else {
      L24 = _false;
   }
   if (L23) {
      L22 = _true;
   } else {
      L22 = L24;
   }
   if (L18) {
      L17 = L22;
   } else {
      L17 = _false;
   }
   PORTE_O_fermer_porte(ctx->client_data, L17);
   L29 = (! L5);
   L28 = (L19 && L29);
   L31 = (L19 && L5);
   if (L31) {
      L30 = L22;
   } else {
      L30 = _false;
   }
   if (L28) {
      L27 = _true;
   } else {
      L27 = L30;
   }
   PORTE_O_porte_ok(ctx->client_data, L27);
}
