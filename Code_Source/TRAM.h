/********
* ec2c version 0.68
* context   method = HEAP
* ext call  method = PROCEDURES
* c header file generated for node : TRAM 
* to be used with : TRAM.c 
********/
#ifndef _TRAM_EC2C_H_FILE
#define _TRAM_EC2C_H_FILE
/*-------- Predefined types ---------*/
#ifndef _EC2C_PREDEF_TYPES
#define _EC2C_PREDEF_TYPES
typedef int _boolean;
typedef int _integer;
typedef char* _string;
typedef double _real;
typedef double _double;
typedef float _float;
#define _false 0
#define _true 1
#endif
/*--------- Pragmas ----------------*/
//MODULE: TRAM 6 5
//IN: _boolean en_station
//IN: _boolean attention_depart
//IN: _boolean demande_porte
//IN: _boolean porte_ouverte
//IN: _boolean demande_pass
//IN: _boolean pass_sortie
//OUT: _boolean ouvrir_porte
//OUT: _boolean fermer_porte
//OUT: _boolean sortir_pass
//OUT: _boolean rentrer_pass
//OUT: _boolean porte_pass_ok
#ifndef _TRAM_EC2C_SRC_FILE
/*--------Context type -------------*/
struct TRAM_ctx;
/*-------- Input procedures -------------*/
extern void TRAM_I_en_station(struct TRAM_ctx* ctx, _boolean);
extern void TRAM_I_attention_depart(struct TRAM_ctx* ctx, _boolean);
extern void TRAM_I_demande_porte(struct TRAM_ctx* ctx, _boolean);
extern void TRAM_I_porte_ouverte(struct TRAM_ctx* ctx, _boolean);
extern void TRAM_I_demande_pass(struct TRAM_ctx* ctx, _boolean);
extern void TRAM_I_pass_sortie(struct TRAM_ctx* ctx, _boolean);
/*-------- Reset procedure -----------*/
extern void TRAM_reset(struct TRAM_ctx* ctx);
/*--------Context copy -------------*/
extern void TRAM_copy_ctx(struct TRAM_ctx* dest, struct TRAM_ctx* src);
/*--------Context allocation --------*/
extern struct TRAM_ctx* TRAM_new_ctx(void* client_data);
/*-------- Step procedure -----------*/
extern void TRAM_step(struct TRAM_ctx* ctx);
#endif
#endif
