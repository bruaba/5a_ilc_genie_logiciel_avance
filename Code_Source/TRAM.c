/********
* ec2c version 0.68
* c file generated for node : TRAM 
* context   method = HEAP
* ext call  method = PROCEDURES
********/
#include <stdlib.h>
#include <string.h>
#define _TRAM_EC2C_SRC_FILE
#include "TRAM.h"
/*--------
Internal structure for the call
--------*/
typedef struct  {
   void* client_data;
   //INPUTS
   _boolean _en_station;
   _boolean _attention_depart;
   _boolean _demande_porte;
   _boolean _porte_ouverte;
   _boolean _demande_pass;
   _boolean _pass_sortie;
   //OUTPUTS
   _boolean _ouvrir_porte;
   _boolean _fermer_porte;
   _boolean _sortir_pass;
   _boolean _rentrer_pass;
   _boolean _porte_pass_ok;
   //REGISTERS
} TRAM_ctx;
/*--------
Output procedures must be defined,
Input procedures must be used:
--------*/
void TRAM_I_en_station(TRAM_ctx* ctx, _boolean V){
   ctx->_en_station = V;
}
void TRAM_I_attention_depart(TRAM_ctx* ctx, _boolean V){
   ctx->_attention_depart = V;
}
void TRAM_I_demande_porte(TRAM_ctx* ctx, _boolean V){
   ctx->_demande_porte = V;
}
void TRAM_I_porte_ouverte(TRAM_ctx* ctx, _boolean V){
   ctx->_porte_ouverte = V;
}
void TRAM_I_demande_pass(TRAM_ctx* ctx, _boolean V){
   ctx->_demande_pass = V;
}
void TRAM_I_pass_sortie(TRAM_ctx* ctx, _boolean V){
   ctx->_pass_sortie = V;
}
extern void TRAM_O_ouvrir_porte(void* cdata, _boolean);
extern void TRAM_O_fermer_porte(void* cdata, _boolean);
extern void TRAM_O_sortir_pass(void* cdata, _boolean);
extern void TRAM_O_rentrer_pass(void* cdata, _boolean);
extern void TRAM_O_porte_pass_ok(void* cdata, _boolean);
#ifdef CKCHECK
extern void TRAM_BOT_ouvrir_porte(void* cdata);
extern void TRAM_BOT_fermer_porte(void* cdata);
extern void TRAM_BOT_sortir_pass(void* cdata);
extern void TRAM_BOT_rentrer_pass(void* cdata);
extern void TRAM_BOT_porte_pass_ok(void* cdata);
#endif
/*--------
Internal reset input procedure
--------*/
static void TRAM_reset_input(TRAM_ctx* ctx){
   //NOTHING FOR THIS VERSION...
}
/*--------
Reset procedure
--------*/
void TRAM_reset(TRAM_ctx* ctx){
   TRAM_reset_input(ctx);
}
/*--------
Copy the value of an internal structure
--------*/
void TRAM_copy_ctx(TRAM_ctx* dest, TRAM_ctx* src){
   memcpy((void*)dest, (void*)src, sizeof(TRAM_ctx));
}
/*--------
Dynamic allocation of an internal structure
--------*/
TRAM_ctx* TRAM_new_ctx(void* cdata){
   TRAM_ctx* ctx = (TRAM_ctx*)calloc(1, sizeof(TRAM_ctx));
   ctx->client_data = cdata;
   TRAM_reset(ctx);
   return ctx;
}
/*--------
Step procedure
--------*/
void TRAM_step(TRAM_ctx* ctx){
//LOCAL VARIABLES
   _boolean L11;
   _boolean L10;
   _boolean L9;
   _boolean L8;
   _boolean L7;
   _boolean L16;
   _boolean L17;
   _boolean L15;
   _boolean L14;
   _boolean L6;
   _boolean L22;
   _boolean L21;
   _boolean L23;
   _boolean L20;
   _boolean L25;
   _boolean L27;
   _boolean L26;
   _boolean L24;
   _boolean L19;
   _boolean L31;
   _boolean L30;
   _boolean L34;
   _boolean L36;
   _boolean L35;
   _boolean L33;
   _boolean L32;
   _boolean L29;
   _boolean L40;
   _boolean L39;
   _boolean L42;
   _boolean L41;
   _boolean L38;
   _boolean L49;
   _boolean L48;
   _boolean L47;
   _boolean L51;
   _boolean L50;
   _boolean L46;
   _boolean L55;
   _boolean L54;
   _boolean L53;
   _boolean L57;
   _boolean L56;
   _boolean L52;
   _boolean L45;
   _boolean L44;
//CODE
   if (ctx->_attention_depart) {
      L11 = _true;
   } else {
      L11 = _false;
   }
   L10 = (! L11);
   if (L10) {
      L9 = _true;
   } else {
      L9 = _false;
   }
   L8 = (ctx->_demande_porte && L9);
   if (L8) {
      L7 = _true;
   } else {
      L7 = _false;
   }
   L16 = (ctx->_en_station && ctx->_demande_porte);
   L17 = (! ctx->_porte_ouverte);
   L15 = (L16 && L17);
   if (L15) {
      L14 = _true;
   } else {
      L14 = _false;
   }
   if (L7) {
      L6 = L14;
   } else {
      L6 = _false;
   }
   TRAM_O_ouvrir_porte(ctx->client_data, L6);
   L22 = (L11 && ctx->_en_station);
   if (L22) {
      L21 = _true;
   } else {
      L21 = _false;
   }
   L23 = (ctx->_porte_ouverte || ctx->_demande_porte);
   L20 = (L21 && L23);
   L25 = (ctx->_en_station && ctx->_porte_ouverte);
   L27 = (! ctx->_en_station);
   if (L27) {
      L26 = _true;
   } else {
      L26 = _false;
   }
   if (L25) {
      L24 = _true;
   } else {
      L24 = L26;
   }
   if (L20) {
      L19 = L24;
   } else {
      L19 = _false;
   }
   TRAM_O_fermer_porte(ctx->client_data, L19);
   L31 = (ctx->_demande_pass && L9);
   if (L31) {
      L30 = _true;
   } else {
      L30 = _false;
   }
   L34 = (ctx->_en_station && ctx->_demande_pass);
   L36 = (! ctx->_pass_sortie);
   L35 = (L17 && L36);
   L33 = (L34 && L35);
   if (L33) {
      L32 = _true;
   } else {
      L32 = _false;
   }
   if (L30) {
      L29 = L32;
   } else {
      L29 = _false;
   }
   TRAM_O_sortir_pass(ctx->client_data, L29);
   L40 = (ctx->_pass_sortie || ctx->_demande_pass);
   L39 = (L21 && L40);
   L42 = (ctx->_en_station && ctx->_pass_sortie);
   if (L42) {
      L41 = _true;
   } else {
      L41 = L26;
   }
   if (L39) {
      L38 = L41;
   } else {
      L38 = _false;
   }
   TRAM_O_rentrer_pass(ctx->client_data, L38);
   L49 = (! L7);
   L48 = (L21 && L49);
   L47 = (L48 && L17);
   L51 = (L21 && L7);
   if (L51) {
      L50 = L24;
   } else {
      L50 = _false;
   }
   if (L47) {
      L46 = _true;
   } else {
      L46 = L50;
   }
   L55 = (! L30);
   L54 = (L21 && L55);
   L53 = (L54 && L36);
   L57 = (L21 && L30);
   if (L57) {
      L56 = L41;
   } else {
      L56 = _false;
   }
   if (L53) {
      L52 = _true;
   } else {
      L52 = L56;
   }
   L45 = (L46 && L52);
   if (L45) {
      L44 = _true;
   } else {
      L44 = _false;
   }
   TRAM_O_porte_pass_ok(ctx->client_data, L44);
}
